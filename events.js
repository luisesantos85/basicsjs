//
/*
* Event capturing phase: Event goes from the root/window to the target
* Event Bubbling phase: Event goes from the target to the window object or the root
*
* You can set event listeners to execute in the capturing or the bubbling phase.
* By default uses bubbling phase
*
*
* Difference stopPropagation and preventDefault
* stopPropagation: prevents the event to keep propagating either in the capturing phase and the bubbling phase but the target still behaves as expected
* preventDefault: doesn't stop propagation but the target does not behave as expected
* */