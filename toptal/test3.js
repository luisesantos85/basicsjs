function log(a){
    console.log(a);
}

function solution(A) {
    let result = 1;
    let minInRange = Math.MAX_VALUE;
    let maxInRange = Math.MIN_VALUE;

    for(let n of A){
        if(n < maxInRange)
            maxInRange = n;
        if(n > minInRange)
            minInRange = n;

        if(n < minInRange)
            if(result > 1)
                result--;
        if(n > maxInRange)
            result++;
    }

    return result;
}

log(solution([2, 4, 1, 6, 5, 9, 7]));
//log(solution([4, 3, 2, 6, 1]));
//log(solution([2, 1, 6, 4, 3, 7]));
