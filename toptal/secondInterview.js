function log(A){
    console.log(A);
}

function solution(M, P){
    let result = [0, 0, 0, 0, 0, 0];
    let ammount = P;
    while(ammount < M){
        if(ammount + 1 <= M) {
            ammount += 1;
            result[5] += 1;
            continue;
        }
        if(ammount + 0.5 <= M) {
            ammount += 0.5;
            result[4] += 1;
            continue;
        }
        if(ammount + 0.25 <= M) {
            ammount += 0.25;
            result[3] += 1;
            continue;
        }
        if(ammount + 0.1 <= M) {
            ammount += 0.1;
            result[2] += 1;
            continue;
        }
        if(ammount + 0.05 <= M) {
            ammount += 0.05;
            result[1] += 1;
            continue;
        }
        if(ammount + 0.01 <= M) {
            ammount += 0.01;
            result[0] += 1;
            continue;
        }
        break;
    }
    return result;
}

log(solution(0.45, 0.34));
//log(solution(5, 0.99));