function log(a){
    console.log(a);
}

class Point2D {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

function solution(A) {
    let Q1 = [];
    let Q2 = [];
    let Q3 = [];
    let Q4 = [];

    for(let point of A){
        if(point.x > 0 && point.y > 0)
            Q1.push(point);
        if(point.x < 0 && point.y > 0)
            Q2.push(point);
        if(point.x < 0 && point.y < 0)
            Q3.push(point);
        if(point.x > 0 && point.y < 0)
            Q4.push(point);
    }

    return calculateSlopes(Q1) + calculateSlopes(Q2) + calculateSlopes(Q3) + calculateSlopes(Q4);
}

function calculateSlopes(Q){
    let set = new Set();
    let slope;
    for(let point of Q){
        slope = point.y / point.x;
        if(!set.has(slope))
            set.add(slope);
    }
    return set.size;
}

let p1 = new Point2D(-1, -2);
let p2 = new Point2D(1, 2);
let p3 = new Point2D(2, 4);
let p4 = new Point2D(-3, 2);
let p5 = new Point2D(2, -2);

log(solution([p1, p2, p3, p4, p5]));