function log(a){
    console.log(a);
}

function solution(A, B, C) {
    //Bad O(n) needs to be reviewed
    // Maybe when i sort i could check in B where the max number in B from the max in A
    let result = 0;

    A.sort((a,b) => a-b);
    B.sort((a,b) => a-b);
    C.sort((a,b) => a-b);

    for(let itemA of A){
        for(let itemB of B){
            if(itemB <= itemA)
                continue;
            for(let itemC of C){
                if(itemC <= itemB)
                    continue;
                result++;
                if(result > 1000000000)
                    return -1;
            }
        }
    }

    return result;
}


log(solution([29, 50], [61, 37], [37, 70]));
log(solution([29, 29], [61, 61], [70, 70]));
log(solution([5], [5], [5]));