"use strict";

//Prototype inheritance

//Prototype is an object that is shared by other objects of the same kind

var animal = {
    kind: 'person'
};
var asim = {};
asim._proto_ = animal;

console.log(asim.kind); //prints person
console.log(animal.isPrototypeOf(asim)); //prints true

asim.kind = 'igloo';
console.log(asim.kind); //prints igloo
console.log(animal.kind); //prints person. When making changes to the object it doesn't change the proto object

// _proto_ should not be used in production because some browsers don't use it

// recomended way to create objects when passing the prototype is:

var luis = Object.create(animal, /*default properties descriptor object*/ {food: {value: 'mango'}});


//Difference between classical and prototypal inheritance

//Classic: you have classes that are the blueprints and objects that are instances based on those blueprints

//Prototypal: Everything is and object and objects of the same kind share a common object



//In Javascript Pseudo-Classical Patter (a patter to write something quite similar to classical inheritance) or Prototypal Pattern (Using the prototypes in a more direct way)

//Pseudo Classical:

//Function constructors and new operator

function Person(firstName, lastName){
    this.firstName = firstName; //this line would give you an error since we are using "use strict" mode unless we use the keyword new
    this.lastName = lastName;

    // There is a big benefit of declaring a function inside Person body and not in the prototype and that is you can simulate private properties.
    // Thats because you can return firstName + ' ' + lastName; from here and those are going to be taken from the parameters instead of this
    this.fullName = function(){
        return this.firstName + ' ' + this.lastName;
    };

    this.fullNamePrivate = function(){
        return firstName + ' ' + lastName;
    }
}

Person.prototype.fullNameProto = function(){
    return this.firstName + ' ' + this.lastName;
};

//var dude = Person('luis', 'santos'); //throws an error because this = undefined in strict mode
var dude = new Person('luis', 'santos');

//this is similar to the following

var dude2 = {};
Person.call(dude2, "luis", "santos");

//benefit of prototype: saves in memory because every instances share functionality


//INHERITANCE in Pseudo classical

function Professional(title, firstName, lastName){
    Person.call(this, firstName, lastName);
    this.title = title;
}

Professional.prototype = Object.create(Person.prototype); //This is the magic. Object.create creates a new object which has as prototype the object pass as argument

Professional.prototype.professionalName = function(){
    return this.title + ' ' + this.firstName + ' ' + this.lastName;
};

var prof = new Professional('dr', 'luis', 'santos');
console.log(prof.professionalName());
console.log(prof.fullName());




//Prototypal Inheritance: You don't use the new operator

//Benefit: working with the tools that Javascript offers instead of simulating something else

var PersonProto = {
    fullName: function(){
        return this.firstName + " " + this.lastName;
    }
};

var asim2 = Object.create(PersonProto, {firstName :{value: 'luis'}, lastName: {value: 'santos'}});
console.log(asim2);
console.log(asim2.fullName());

// or we can use factories

function PersonFactory(firstName, lastName){
    var person = Object.create(PersonProto);
    person.firstName = firstName;
    person.lastName = lastName;
    return person;
}

var other = PersonFactory('luis', 'santos');

//INHERITANCE

var ProfessionalProto = Object.create(PersonProto);
var luisProfesional = Object.create(ProfessionalProto);

//Benefits of Constructor patter
//Most natural to users on other languages, private properties, most widely used

//Benefits of prototype patters
//Easier to learn and feels more natural to javascript



/**
 * ES6
 * At its most basic level, the class keyword in ES6 is equivalent to a constructor function definition that conforms to prototype-based inheritance
 * */