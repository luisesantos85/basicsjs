//https://hackernoon.com/how-to-implement-dijkstras-algorithm-in-javascript-abdfd1702d04

/**
 * DIJKSTRA
 *
 * 1- Find the “cheapest” node. (start)
 * 2- Update the costs of the immediate neighbors of this node.
 * 3- Repeat steps 1 and 2 until you’ve done this for every node.
 * 4- Return the lowest cost to reach the node, and the optimal path to do so.
 * */

const graph = {
    start: {A: 5, B: 2},
    A: {C: 4, D: 2},
    B: {A: 8, D: 7},
    C: {D: 6, finish: 3},
    D: {finish: 1},
    finish: {}
};


/**
 * Given the costs and the processed nodes, will return the cheapest node
 * that hasn’t been processed.
 * */
const lowestCostNode = (costs, processed) => {
    return Object.keys(costs).reduce((lowest, node) => {
        if (lowest === null || costs[node] < costs[lowest]) {
            if (!processed.includes(node)) {
                lowest = node;
            }
        }
        return lowest;
    }, null);
};


const dijkstra = (graph) => {
    const costs = {finish: Infinity, ...graph.start};
    const parents = {finish: null};

    for (let child in graph.start) {  // add children of start node
        parents[child] = 'start';
    }
    const processed = [];

    let node = lowestCostNode(costs, processed);
    while (node) {
        let cost = costs[node];
        let children = graph[node];
        for (let n in children) {
            let newCost = cost + children[n];
            if (!costs[n]) {
                costs[n] = newCost;
                parents[n] = node;
            }
            if (costs[n] > newCost) {
                costs[n] = newCost;
                parents[n] = node;
            }
        }
        processed.push(node);
        node = lowestCostNode(costs, processed);
    }

    //Dijkstra should return costs

    let optimalPath = ['finish'];
    let parent = parents.finish;
    while (parent) {
        optimalPath.push(parent);
        parent = parents[parent];
    }

    optimalPath.reverse();  // reverse array to get correct order
    const results = {
        distance: costs.finish,
        path: optimalPath
    };
    return results;
};

console.log(dijkstra(graph));