class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
        this.prev = null; //This is only for Doubled Linked List
    }
}

module.exports = Node;