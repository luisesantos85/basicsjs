class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    distance(point){
        let a = this.x - point.x;
        let b = this.y - point.y;

        return Math.sqrt( a*a + b*b );
    }
}

function getSum(total, num) {
    return total + num;
}

function solution(p1, p2, p3) {
    let distances = [p1.distance(p2), p1.distance(p3), p2.distance(p3)];
    return Number(distances.reduce(getSum)/distances.length).toFixed(2);
}

let p1 = new Point(2, 5);
let p2 = new Point(1, 4);
let p3 = new Point(6, 2);

console.log(solution(p1,p2,p3));


