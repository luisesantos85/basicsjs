class Stack {
    constructor() {
        this.data = [];
    }

    push(record) {
        this.data.push(record);
    }

    pop() {
        return this.data.pop();
    }
}

class Item {
    constructor(openIndex) {
        this.openIndex = openIndex;
    }
}

function log(openIndex, closeIndex){
    console.log(openIndex + ":" + closeIndex);
}

function solution(A, index) {
    let open = {'[':']'};
    let close = {']':'[', };

    const stack = new Stack();
    let a = A.split("");

    for(let i=0; i < a.length; i++){
        let char = a[i];
        if(open[char]) {
            stack.push(new Item(i, char));
        }else if(close[char]){
            let item = stack.pop();
            if(item.openIndex === index){
                log(item.openIndex, i);
                return;
            }
        }
    }
}

solution("[ABC[23]][89]", 1);


