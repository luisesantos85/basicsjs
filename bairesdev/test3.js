
function isAlmostPalindrome(word) {

    let reversed = word.split('').reverse().join('');

    if(reversed === word)
        return true;
    else{
        let counter = 0;
        for(let i = 0; i<word.length; i++){
            if(word.charAt(i) !== reversed.charAt(i)){
                counter++
                if(counter > 2)
                    return false;
            }
        }
        return true;
    }
}



console.log(isAlmostPalindrome("rotator"));
console.log(isAlmostPalindrome("kayak"));
console.log(isAlmostPalindrome("father"));
console.log(isAlmostPalindrome("abccbx"));
console.log(isAlmostPalindrome("abccfg"));


