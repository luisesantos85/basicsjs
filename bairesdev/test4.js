
function solution(A) {

    let quantities = {};

    for(let item of A ){
        if(quantities[item])
            quantities[item]++;
        else
            quantities[item] = 1;
    }

    let max = Number.MIN_VALUE;
    for(let key in quantities) {
        if(quantities[key] > max)
            max = quantities[key];
    }

    return max;
}

console.log(solution([1,2,3,2,3,4]));



