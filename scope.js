"use strict"; // It is used to tell undeclared variables can't be used and to set the compiler to Strict Operating Context

// all global variables are properties of the global object (in the browser is window)

// In Javascript there are 2 type of scopes: Global or Function scopes. There is nothing such a block scope unless you use let
for(var i = 0; i<5 ; i++){
    var j = 2;
    console.log(j);
}

// Variable / Fuction Hoisting or variable elevation
// Javascript compiler put variable and function definition at the top of the scope when it finds var a = 1 or a function;

function hoist(){
    console.log(h);
    var h = 1;   // You may expect this to throw an error but the compiler translate this to the following function:
}

function hoistCompiled(){
    var h;
    console.log(h);
    h = 1;
}

//Scope Chain
// Function scope can be nested into another function scope.
// The scope function is defined lexically

function goo(){
    var myVar = 1;
    function foo(){
        console.log(myVar);
    }
    foo();
}
goo();  // This prints 1 but if function foo was declared outside function goo it would throw an undeclared variable error


// IIFE : Immediate Invoked function expression. You just define a function and call it right away. useful to keep variables
//  and functions inside a function scope so you don't pollute the global scope
(function(){
    var thing = {'hello': 'other'};
    console.log(thing);
})();


//Closures
function sayHello(name){
    var text = "Hello " + name;
    return function (){
        console.log(text);
    }
}
var sh = sayHello('Luis');
sh();

// Remember the scope chain is defined lexically but the ONLY EXCEPTION is with Closures. Closures means when a function returns a functions and the returned function keeps the scope of its parent function

//Interesting example
var arr = [];
for (var k = 0; k < 10 ; k++){
    arr[k] = function(){return k}
}
console.log(arr[0]());
console.log(arr[1]());
console.log(arr[2]());
//This prints 10, 10, 10


//Interesting example
var salary = 1000;
(function () {
    console.log('my old salary is ' + salary);
    var salary = 5000;
    console.log('my new salary is ' + salary);
})();
//This prints undefined and 5000 because of variable Hoisting


//Value of this: The owner of the context the code its being executed at or another definition is the owner of the calling context
//"use strict";
var asim = {
    checkThis: function(){
        console.log(this);
        function checkOther(){
            this.moo = 1; // here I am setting moo on the window object unless I use "use strict"
            console.log(this);
        }
        checkOther();
    }
};
asim.checkThis();
//This function prints asim Object and Window. If there is no Owner this is equal to the global object or undefined if we are using "use strict" mode. For callbacks the value of this is the global object

function luis() {
    console.log(this); // This should return undefined in a normal situation because we are using "use strict" mode
}
luis.call(this); // when using function.call you can pass the value of the context so this would print Window in the console


//luis() is equal to luis.call(luis). If you don't have a caller it will be equal to luis.call(window) with "use strict" mode disabled or luis.call(undefined)  with "use strict" mode enabled
//function.call can accept parameters

function luis2(a, b) {
    console.log(this);
}
luis2.call(this, 1, 2);

//Using apply

luis2.apply(this,[1, 2]); //Pass parameters in an array. You use this if you are using the arguments array within the function

//functions don't have to declare all the parameters pass to them
function sum() {
    var total = 0;
    for (var i = 0; i< arguments.length; i++){
        total += arguments[i];
    }
    return total;
}

// Using bind:Returns a new function that is binded to the specified owner. Sets the value of this without executing the function

var bi = function(){
    console.log(this);
}.bind(1);

bi();