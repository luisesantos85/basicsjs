//CORS
/*
* When sending a get request the response get blocked by the browser if the origin of the request is different to the api domain
* The API could return a Access-Control-Allow-Origin: {origin} to solve this issue
*
* for PUT, POST, DELETE sor security reasons you could send and prefly OPTIONS request to ask the server if PUT is allowed
*
*  Access-Control-Request-Method : PUT
*
* That s going to return the methods allowed to use for that particular API
*
* */

//JSONP
/*
* Works just with  GET request
* wraps the json into a function
* when the response comes it tries to execute a function with the same name of the function that wraps the response
* */