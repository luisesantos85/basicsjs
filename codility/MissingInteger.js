// Write a function:
//
// function solution(A);
//
// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
//
//     For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
//
// Given A = [1, 2, 3], the function should return 4.
//
// Given A = [−1, −3], the function should return 1.
//
// Assume that:
//
//     N is an integer within the range [1..100,000];
// each element of array A is an integer within the range [−1,000,000..1,000,000].
// Complexity:
//
//     expected worst-case time complexity is O(N);
// expected worst-case space complexity is O(N) (not counting the storage required for input arguments).


function log(a){
    console.log(a);
}

function solution(A) {
    let aux = 0;
    A.sort((a, b) => a - b);

    if(A[0] > 1)
        return 1;

    let foundPositive = false;

    for(let i = 0; i < A.length - 1 ; i++){
        if(A[i] > 0){
            foundPositive = true;
            aux = A[i+1] - A[i];
            if(aux > 1)
                return A[i]+1;
        }
    }

    if(!foundPositive)
        return 1;
    return A[A.length - 1] + 1;
}

log(solution([-1, -2, -3]));