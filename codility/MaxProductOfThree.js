// A non-empty array A consisting of N integers is given. The product of triplet (P, Q, R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).
//
// For example, array A such that:
//
//     A[0] = -3
// A[1] = 1
// A[2] = 2
// A[3] = -2
// A[4] = 5
// A[5] = 6
// contains the following example triplets:
//
//     (0, 1, 2), product is −3 * 1 * 2 = −6
// (1, 2, 4), product is 1 * 2 * 5 = 10
// (2, 4, 5), product is 2 * 5 * 6 = 60
// Your goal is to find the maximal product of any triplet.
//
//     Write a function:
//
// function solution(A);
//
// that, given a non-empty array A, returns the value of the maximal product of any triplet.
//
//     For example, given array A such that:
//
//     A[0] = -3
// A[1] = 1
// A[2] = 2
// A[3] = -2
// A[4] = 5
// A[5] = 6
// the function should return 60, as the product of triplet (2, 4, 5) is maximal.
//
//     Assume that:
//
//     N is an integer within the range [3..100,000];
// each element of array A is an integer within the range [−1,000..1,000].
// Complexity:
//
//     expected worst-case time complexity is O(N*log(N));
// expected worst-case space complexity is O(1) (not counting the storage required for input arguments).

function log(a){
    console.log(a);
}

function solution(A) {
    // write your code in JavaScript (Node.js 8.9.4)
    let N = A.length;

    if(N===3){
        return A[0]*A[1]*A[2];
    }else{
        A.sort((a,b) => a-b); //Be careful with sort() because by default it sorts elements as strings. You have to pass that function if you want to sort them as numbers
        if(A[0] >= 0 || A[N-1]<0)
            return A[N-1]*A[N-2]*A[N-3];

        let left = A[0]*A[1];
        let right = A[N-1]*A[N-2];

        if(N===4){
            return Math.max(left*A[N-2],left*A[N-1], right*A[0],right*A[1]);
        }else{
            return Math.max(left*A[2], left*A[N-3], left*A[N-2], left*A[N-1], right*A[2], right*A[N-3], right*A[0], right*A[1]);
        }
    }
}

log(solution([4, 7, 3, 2, 1, -3, -5]));
log(solution([-1000, 2, 3, 100]));
log(solution([-3,1,2,-2,5,6]));
log(solution([-5, -6, -4, -7, -10]));
log(solution([-5, 5, -5, 4]));
