// An array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
//
//     Your goal is to find that missing element.
//
//     Write a function:
//
// function solution(A);
//
// that, given an array A, returns the value of the missing element.
//
//     For example, given array A such that:
//
//     A[0] = 2
// A[1] = 3
// A[2] = 1
// A[3] = 5
// the function should return 4, as it is the missing element.
//
//     Assume that:
//
//     N is an integer within the range [0..100,000];
// the elements of A are all distinct;
// each element of array A is an integer within the range [1..(N + 1)].
//     Complexity:
//
// expected worst-case time complexity is O(N);
// expected worst-case space complexity is O(1) (not counting the storage required for input arguments).

function log(a){
    console.log(a);
}

function solution(A) {
    // write your code in JavaScript (Node.js 8.9.4)
    let d={};
    for(let i = 1;i<=A.length+1;i++){
        d[i] = '-';
    }
    for(let i = 0;i<A.length;i++){
        delete d[A[i]];
    }

    for(let key in d){
        return parseInt(key);
    }

}

log(solution([1, 2, 3 , 5, 6]));