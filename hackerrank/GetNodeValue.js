// You’re given the pointer to the head node of a linked list and a specific position. Counting backwards from the tail node of the linked list, get the value of the node at the given position. A position of 0 corresponds to the tail, 1 corresponds to the node before the tail and so on.

// Input Format

// You have to complete the int getNode(SinglyLinkedListNode* head, int positionFromTail) method which takes two arguments - the head of the linked list and the position of the node from the tail. positionFromTail will be at least 0 and less than the number of nodes in the list. You should NOT read any input from stdin/console.

// The first line will contain an integer , the number of test cases. 
// Each test case has the following format: 
// The first line contains an integer , the number of elements in the linked list. 
// The next  lines contains, an element each denoting the element of the linked list. 
// The last line contains an integer  denoting the position from the tail, whose value needs to be found out and returned.

// Constraints

// , where  is the  element of the linked list.
// Output Format

// Find the node at the given position counting backwards from the tail. Then return the data contained in this node. Do NOT print anything to stdout/console.

// The code in the editor handles output. 
// For each test case, print the value of the node, each in a new line.

// Sample Input

// 2
// 1
// 1
// 0
// 3
// 3
// 2
// 1
// 2
// Sample Output

// 1
// 3
// Explanation

// In first case, there is one element in linked list with value 1. Hence, last element is 1.

// In second case, there are 3 elements with values 3, 2 and 1 (3 -> 2 -> 1). Hence, element with position of 2 from tail is 3.

'use strict';

const fs = require('fs');

const Node = require('../structures/node');
const LinkedList = require('../structures/linkedlist');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

function printSinglyLinkedList(node, sep, ws) {
    while (node != null) {
        ws.write(String(node.data));

        node = node.next;

        if (node != null) {
            ws.write(sep);
        }
    }
}

function getNode(head, positionFromTail) {

    let fast = head;
    let slow = head;

    if(positionFromTail > 0){
        for(let i = 0; i<positionFromTail;i++)
            fast = fast.next;
    }
    
    while(fast.next){
        slow = slow.next;
        fast = fast.next;
    }    

    return slow.data;
}

function main() {
    const ws = fs.createWriteStream('./hackerrank/output.txt');

    const tests = parseInt(readLine(), 10);

    for (let testsItr = 0; testsItr < tests; testsItr++) {
        const llistCount = parseInt(readLine(), 10);

        let llist = new LinkedList();

        for (let i = 0; i < llistCount; i++) {
            const llistItem = parseInt(readLine(), 10);
            llist.insertNode(llistItem);
        }

        const position = parseInt(readLine(), 10);

        let result = getNode(llist.head, position);

        ws.write(result + "\n");
    }

    ws.end();
}