// Given a reference to the head of a doubly-linked list and an integer, , create a new DoublyLinkedListNode object having data value  and insert it into a sorted linked list.

// Complete the DoublyLinkedListNode SortedInsert(DoublyLinkedListNode head, int data) method in the editor below. It has two parameters:

// : A reference to the head of a doubly-linked list of Node objects.
// : An integer denoting the value of the  field for the Node you must insert into the list.
// The method must insert a new Node into the sorted (in ascending order) doubly-linked list whose data value is  without breaking any of the list's double links or causing it to become unsorted.

// Note: Recall that an empty list (i.e., where ) and a list with one element are sorted lists.

// Input Format

// The first line contains an integer , the number of test cases.

// Each of the test case is in the following format:

// The first line contains an integer , the number of elements in the linked list.
// Each of the next  lines contains an integer, the data for each node of the linked list.
// The last line contains an integer  which needs to be inserted into the sorted doubly-linked list.
// Constraints

// Output Format

// Do not print anything to stdout. Your method must return a reference to the  of the same list that was passed to it as a parameter.

// The ouput is handled by the code in the editor and is as follows: 
// For each test case, print the elements of the sorted doubly-linked list separated by spaces on a new line.

// Sample Input

// 1
// 4
// 1
// 3
// 4
// 10
// 5
// Sample Output

// 1 3 4 5 10
// Explanation

// The initial doubly linked list is:  .

// The doubly linked list after insertion is: 

'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

const DoublyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
        this.prev = null;
    }
};

const DoublyLinkedList = class {
    constructor() {
        this.head = null;
        this.tail = null;
    }

    insertNode(nodeData) {
        let node = new DoublyLinkedListNode(nodeData);

        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
            node.prev = this.tail;
        }

        this.tail = node;
    }
};

function printDoublyLinkedList(node, sep, ws) {
    while (node != null) {
        ws.write(String(node.data));

        node = node.next;

        if (node != null) {
            ws.write(sep);
        }
    }
}

function sortedInsert(head, data) {

    let node = head;
    let aux, newNode;

    if(data <= head.data){
        let newNode = new DoublyLinkedListNode(data);
        head.prev = newNode;
        newNode.next = head;
        return newNode;
    }else{
        while(node.next){
            if(data >= node.data && data < node.next.data){
                newNode = new DoublyLinkedListNode(data);
                aux = node.next;
                node.next = newNode;
                aux.prev = newNode;
                newNode.prev = node;
                newNode.next = aux;
                return head;
            }else{
                node = node.next;
            }
        }
        newNode = new DoublyLinkedListNode(data);
        node.next = newNode;
        newNode.prev = node;
        return head;
    }
}

function main() {
    const ws = fs.createWriteStream('./hackerrank/output.txt');

    const t = parseInt(readLine(), 10);

    for (let tItr = 0; tItr < t; tItr++) {
        const llistCount = parseInt(readLine(), 10);

        let llist = new DoublyLinkedList();

        for (let i = 0; i < llistCount; i++) {
            const llistItem = parseInt(readLine(), 10);
            llist.insertNode(llistItem);
        }

        const data = parseInt(readLine(), 10);

        let llist1 = sortedInsert(llist.head, data);

        printDoublyLinkedList(llist1, " ", ws)
        ws.write("\n");
    }

    ws.end();
}