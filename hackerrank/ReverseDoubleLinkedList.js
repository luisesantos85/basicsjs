
function log(A){
    console.log(A);
}
function arrayManipulation(n, queries) {

    console.log(n);
    console.log(queries);

    let result = new Array(n);
    result.fill(0, 0, n);
    let max = Number.MIN_VALUE;
    console.log(result);
    for(let q of queries){
        let a = q[0] - 1;
        let b = q[1] - 1;
        let k = q[2];

        for(let i = a; i<=b;i++){
            result[i] += k;
            if(result[i] > max)
                max = result[i];
        }
        console.log(result);
    }

    return max;

}

log(arrayManipulation(10, [ [ 2, 6, 8 ], [ 3, 5, 7 ], [ 1, 8, 1 ], [ 5, 9, 15 ] ]));