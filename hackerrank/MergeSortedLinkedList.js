// You’re given the pointer to the head nodes of two sorted linked lists. The data in both lists will be sorted in ascending order. Change the next pointers to obtain a single, merged linked list which also has data in ascending order. Either head pointer given may be null meaning that the corresponding list is empty.

// Input Format

// You have to complete the SinglyLinkedListNode MergeLists(SinglyLinkedListNode headA, SinglyLinkedListNode headB) method which takes two arguments - the heads of the two sorted linked lists to merge. You should NOT read any input from stdin/console.

// The input is handled by the code in the editor and the format is as follows:

// The first line contains an integer , denoting the number of test cases. 
// The format for each test case is as follows:

// The first line contains an integer , denoting the length of the first linked list. 
// The next  lines contain an integer each, denoting the elements of the linked list. 
// The next line contains an integer , denoting the length of the second linked list. 
// The next  lines contain an integer each, denoting the elements of the second linked list.

// Constraints

// , where  is the  element of the list.
// Output Format

// Change the next pointer of individual nodes so that nodes from both lists are merged into a single list. Then return the head of this merged list. Do NOT print anything to stdout/console.

// The output is handled by the editor and the format is as follows:

// For each test case, print in a new line, the linked list after merging them separated by spaces.

// Sample Input

// 1
// 3
// 1
// 2
// 3
// 2
// 3
// 4
// Sample Output

// 1 2 3 3 4 
// Explanation

// The first linked list is: 1 -> 2 -> 3 -> NULL

// The second linked list is: 3 -> 4 -> NULL

// Hence, the merged linked list is: 1 -> 2 -> 3 -> 3 -> 4 -> NULL



'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = ' ';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

function printSinglyLinkedList(node, sep, ws) {
    while (node != null) {
        ws.write(String(node.data));

        node = node.next;

        if (node != null) {
            ws.write(sep);
        }
    }
}

const Node = require('../structures/node');
const LinkedList = require('../structures/linkedlist');


function mergeLists(head1, head2) {
    let result = new LinkedList();

    while(head1 || head2){
        if(!head1){
            result.insertNode(head2.data);
            head2 = head2.next;
            continue;
        }
        if(!head2){
            result.insertNode(head1.data);
            head1 = head1.next;
            continue;
        }

        if(head1.data < head2.data){
            result.insertNode(head1.data);
            head1 = head1.next;
        }else{
            result.insertNode(head2.data);
            head2 = head2.next;
        }
    }
    return result.head;
}


function main() {
    const ws = fs.createWriteStream('./hackerrank/output.txt');

    const tests = parseInt(readLine(), 10);

    for (let testsItr = 0; testsItr < tests; testsItr++) {
        const llist1Count = parseInt(readLine(), 10);

        let llist1 = new LinkedList();

        for (let i = 0; i < llist1Count; i++) {
            const llist1Item = parseInt(readLine(), 10);
            llist1.insertNode(llist1Item);
        }
      
      	const llist2Count = parseInt(readLine(), 10);

        let llist2 = new LinkedList();

        for (let i = 0; i < llist2Count; i++) {
            const llist2Item = parseInt(readLine(), 10);
            llist2.insertNode(llist2Item);
        }

        let llist3 = mergeLists(llist1.head, llist2.head);

        printSinglyLinkedList(llist3, " ", ws)
        ws.write("\n");
    }

    ws.end();
}