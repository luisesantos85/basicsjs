//Global object in the browser is window. In Node is called global

"use strict"; // It is used to tell undeclared variables can't be used and to set the compiler to Strict Operating Context

// The browser automatically creates this variable in the global scope unless I use strict mode
// myVariable = 3;

// With no strict mode enabled you could use reserved words of future javascript versions as variable names
//var let = 1;

// delete: The delete keyword deletes both the value of the property and the property itself. After deletion, the property cannot be used before it is added back again. The delete operator is designed to be used on object properties. It has no effect on variables or functions.

// In strict mode you can not use delete operator to delete variables nor functions

// eval: The eval() function evaluates JavaScript code represented as a string.

// primitive are passed by value and objects are passed be reference

var a = {'moo': ''};
function foo(a){
    a.moo = 'too'; //You can only change properties of an object
    a = {'other': 'other value'} // You can not do that. This is not going to affect variable a at all
}
foo(a);
console.log(a);

// undefined means no value
var b;
console.log(b); // gives undefined

// null is always set by the developer. Never from the compiler. The compiler sets undefine values to undefine
// typeof(null) should be Null but Javascript compiler returns object

console.log(null == undefined); // returns true because to the compiler both of them are equal to empty value

console.log(null === undefined); // returns false because the types are different


//NaN not a number. NaN == NaN is false. Only value in Javascript that when compared to itself is false

isNaN(1); //false
isNaN("1"); //false
isNaN("a"); //true

// isNaN tries to make a variable conversion so is quite useless since isNaN("1") is false

// There is a trick to check if a variable is NaN that is bullet proof
// since NaN == NaN is false you can check the variable to itself
var n = NaN;
console.log(n !== n); // returns true if is NaN


