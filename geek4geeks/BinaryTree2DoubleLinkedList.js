// Given a Binary Tree (BT), convert it to a Doubly Linked List(DLL) In-Place. The left and right pointers
// in nodes are to be used as previous and next pointers respectively in converted DLL. The order of nodes
// in DLL must be same as Inorder of the given Binary Tree. The first node of Inorder traversal (left most node in BT)
// must be head node of the DLL.

const Node = require('../structures/node');
const TreeNode = require('../structures/treenode');

function createTree(){
    let node1 = new TreeNode(1);
    let node2 = new TreeNode(2);
    let node3 = new TreeNode(3);
    let node4 = new TreeNode(4);
    let node5 = new TreeNode(5);
    let node6 = new TreeNode(6);
    let node7 = new TreeNode(7);
    let node8 = new TreeNode(8);
    let node9 = new TreeNode(9);
    let node10 = new TreeNode(10);

    node1.left = node2;
    node1.right = node3;

    node2.left = node4;
    node2.right = node5;

    node3.left = node6;
    node3.right = node7;

    node4.left = node8;
    node4.right = node9;

    node7.right = node10;

    return node1;
}

function printLinkedList(head) {
    while(head){
        console.log(head.data);
        head = head.next;
    }
}

function printTree(head) {
    if(!head)
        return '';

    console.log(head.data);
    printTree(head.left);
    printTree(head.right);
}

function convertToDLL(treeNode){
    if(!treeNode)
        return null;

    let node = new Node(treeNode.data);
    node.prev = convertLeft(treeNode.left);
    if(node.prev)
        node.prev.next = node;
    node.next = convertRight(treeNode.right);
    if(node.next)
        node.next.prev = node;

    return node;
}

function convertLeft(treeNode) {
    if(!treeNode)
        return null;

    let node = new Node(treeNode.data);

    if(treeNode.left) {
        node.prev = convertLeft(treeNode.left);
        node.prev.next = node;
    }
    if(treeNode.right) {
        node.next = convertLeft(treeNode.right);
        node.next.prev = node;
    }
    if(node.next)
        return node.next;
    else
        return node;
}

function convertRight(treeNode) {
    if(!treeNode)
        return null;

    let node = new Node(treeNode.data);

    if(treeNode.left) {
        node.prev = convertRight(treeNode.left);
        node.prev.next = node;
    }
    if(treeNode.right) {
        node.next = convertRight(treeNode.right);
        node.next.prev = node;
    }
    if(node.prev)
        return node.prev;
    else
        return node;
}

let head = createTree();
head = convertToDLL(head);

while(head.prev){
    head = head.prev;
}

printLinkedList(head);

