// If you have an array that you need to search, the simplest way might be to use indexOf(), or perhaps a for() loop.
// Either option will start on the left hand side of the array, and iterate through each item in the Array until your desired value is found.
//
//     Now, compare that with a Binary Search.
//
//     A Binary Search allows you to search a SORTED array by repeatedly splitting the array in half.
//
//     A binary search works by checking if our search value is more than, less than, or equal to the middle value in our array:
//
//     If it’s less than, we can remove the right half of the array.
//     If it’s more than, we can remove the left half of the array.
//     If it’s equal to, we return the value
// The catch here is that the array must be sorted — i.e. the values must be in order for a binary search to work.
//
//     When working with large chunks of data, it is much faster to use a binary search because with each iteration you remove 1/2 of the array’s wrong values, instead of just one wrong value.

// Obviously you cam always use Array.indexOf method but for large collections of data Binary Search may offer a better alternative


function binarySearch (list, value) {
    // initial values for start, middle and end
    let start = 0;
    let stop = list.length - 1;
    let middle = Math.floor((start + stop) / 2);

    // While the middle is not what we're looking for and the list does not have a single item
    while (list[middle] !== value && start < stop) {
        if (value < list[middle]) {
            stop = middle - 1
        } else {
            start = middle + 1
        }

        // recalculate middle on every iteration
        middle = Math.floor((start + stop) / 2)
    }

    // if the current middle item is what we're looking for return it's index, else return -1
    return (list[middle] !== value) ? -1 : middle
}

const list = [2, 5, 8, 9, 13, 45, 67, 99];
console.log(binarySearch(list, 99)); // 7 -> returns the index of the item