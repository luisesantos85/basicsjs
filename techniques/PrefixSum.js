/**
 * Given an array arr[] of size n, its prefix sum array is another array prefixSum[] of same size such that the value of prefixSum[i] is arr[0] + arr[1] + arr[2] … arr[i].
 * */
function prefixSum(A) {
    let i = 0,
        l = A.length,
        P = new Array(l),
        sum = A[0];

    P[0] = sum;

    while (++i < l) {
        sum += A[i];
        P[i] = sum;
    }

    return P;
}