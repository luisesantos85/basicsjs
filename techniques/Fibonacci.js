// The Fibonacci numbers form a sequence of integers defined recursively in the following way.
//     The first two numbers in the Fibonacci sequence are 0 and 1, and each subsequent number
// is the sum of the previous two.


function memoize(fn) {
    const cache = {};
    return function(...args) {
        if (cache[args]) {
            return cache[args];
        }

        const result = fn.apply(this, args);
        cache[args] = result;

        return result;
    };
}

function slowFib(n) {
    if (n < 2) {
        return n;
    }

    return fib(n - 1) + fib(n - 2);
}

const fib = memoize(slowFib);