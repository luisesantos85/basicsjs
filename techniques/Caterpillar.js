// The Caterpillar method is a likeable name for a popular means of solving algorithmic tasks.
//     The idea is to check elements in a way that’s reminiscent of movements of a caterpillar.
//     The caterpillar crawls through the array. We remember the front and back positions of the
//      caterpillar, and at every step either of them is moved forward.
//
//      15.1. Usage example
//
//      Let’s check whether a sequence a0, a1,...,an≠1 (1 ˛ ai ˛ 109) contains a contiguous subsequence
//         whose sum of elements equals s. For example, in the following sequence we are looking
//         for a subsequence whose total equals s = 12.
//         a0 a1 a2 a3 a4 a5 a6
//         6 2 7 4 1 3 6
//         Each position of the caterpillar will represent a dierent
//         contiguous subsequence in which
//         the total of the elements is not greater than s. Let’s initially set the caterpillar on the first
//         element. Next we will perform the following steps:
//             • if we can, we move the right end (front) forward and increase the size of the caterpillar;
//         • otherwise, we move the left end (back) forward and decrease the size of the caterpillar.
//             In this way, for every position of the left end we know the longest caterpillar that covers
//         elements whose total is not greater than s. If there is a subsequence whose total of elements
//         equals s, then there certainly is a moment when the caterpillar covers all its elements.