// The Euclidean algorithm is one of the oldest numerical algorithms still to be in common
// use. It solves the problem of computing the greatest common divisor (gcd) of two positive
// integers.

function gcd(a,b) {
    if (b === 0)
    {return a}
    else
    {return gcd(b, a % b)}
}

console.log(gcd(15, 30));