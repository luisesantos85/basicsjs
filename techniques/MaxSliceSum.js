// Or Mas SubArray
// This problem can be solved in O(n). For each position, we compute the largest sum that
// ends in that position. If we assume that the maximum sum of a slice ending in position i equals
// max_ending, then the maximum slice ending in position i+1 equals max(0, max_ending+ ai+1).

//This algorithm is called Kadane's algorithm and is based in Dynamic programming

function maxSliceSum(A){
   let maxSoFar = A[0];
   let maxEndingHere = A[0];

   for(let i = 1; i < A.length; i++){
       maxEndingHere = maxEndingHere + A[i];
       maxEndingHere = Math.max(maxEndingHere, A[i]);

       maxSoFar = Math.max(maxSoFar, maxEndingHere);
   }
   return maxSoFar;
}

console.log(maxSliceSum([-2,2,3,4,-2,1]));