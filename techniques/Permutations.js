// Permutations
//
// When the order matters it is a Permutation.
//
// "The combination to the safe is 472". We do care about the order. 724 won't work, nor will 247. It has to be exactly 4-7-2.
//
// Permutations without repetitions
// A permutation, also called an “arrangement number” or “order”, is a rearrangement of the elements of an ordered list S into a one-to-one correspondence with S itself.
//
//     Below are the permutations of string ABC.
//
//     ABC ACB BAC BCA CBA CAB
//
// Or for example the first three people in a running race: you can't be first and second.
//
// Number of combinations
//
// n * (n-1) * (n -2) * ... * 1 = n!
//     Permutations with repetitions
//     When repetition is allowed we have permutations with repetitions. For example the the lock below: it could be 333



function log(A){
    console.log(A);
}

/**
 * Swap Characters at position
 * @param a string value
 * @param i position 1
 * @param j position 2
 * @return swapped string
 */
function swap(a, i, j) {
    let temp;
    let charArray = a.split('');
    temp = charArray[i] ;
    charArray[i] = charArray[j];
    charArray[j] = temp;
    return charArray.join('');
}

/**
 * permutation function
 * @param str string to calculate permutation for
 * @param l starting index
 * @param r end index
 */
function permute(str, l, r) {
    if (l === r)
        log(str);
    else {
        for (let i = l; i <= r; i++) {
            str = swap(str,l,i);
            permute(str, l+1, r);
            str = swap(str,l,i);
        }
    }
}

let abc = 'ABC';
permute('ABC', 0, abc.length - 1);
